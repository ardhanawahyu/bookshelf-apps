let booksData = localStorage.getItem("booksData");
if (!booksData) {
  booksData = [];
  localStorage.setItem("booksData", JSON.stringify(booksData));
} else {
  booksData = JSON.parse(booksData);
}

const inputBookForm = document.getElementById("inputBook");
inputBookForm.addEventListener("submit", (event) => {
  event.preventDefault();
  const id = +new Date();
  const title = document.getElementById("inputBookTitle").value;
  const author = document.getElementById("inputBookAuthor").value;
  const year = document.getElementById("inputBookYear").value;
  const isComplete = document.getElementById("inputBookIsComplete").checked;

  const newBook = {
    id,
    title,
    author,
    year: Number(year),
    isComplete,
  };

  booksData.push(newBook);
  localStorage.setItem("booksData", JSON.stringify(booksData));

  renderBookList();
  inputBookForm.reset();
});

const renderBookList = (books = booksData) => {
  const incompleteBookshelfList = document.getElementById(
    "incompleteBookshelfList"
  );
  const completeBookshelfList = document.getElementById(
    "completeBookshelfList"
  );

  incompleteBookshelfList.innerHTML = "";
  completeBookshelfList.innerHTML = "";

  books.forEach((book) => {
    const bookHTML = `
    <article class="book_item">
      <h3>${book.title}</h3>
      <p>Penulis: ${book.author}</p>
      <p>Tahun: ${book.year}</p>

      <div class="action">
        <button class="${book.isComplete ? "red" : "green"}" data-id="${
      book.id
    }">${book.isComplete ? "Belum selesai di Baca" : "Selesai dibaca"}</button>
        <button class="red" data-id="${book.id}">Hapus buku</button>
      </div>
    </article>
    `;

    if (book.isComplete) {
      completeBookshelfList.innerHTML += bookHTML;
    } else {
      incompleteBookshelfList.innerHTML += bookHTML;
    }
  });

  const updateBookStatus = document.querySelectorAll(
    ".action button:first-child"
  );
  updateBookStatus.forEach((button) => {
    button.addEventListener("click", (e) => {
      const id = e.target.getAttribute("data-id");
      const bookIndex = booksData.findIndex((book) => book.id === parseInt(id));
      if (bookIndex !== -1) {
        booksData[bookIndex].isComplete = !booksData[bookIndex].isComplete;
        localStorage.setItem("booksData", JSON.stringify(booksData));
        renderBookList();
      }
    });
  });

  const deleteBook = document.querySelectorAll(".action button:last-child");
  deleteBook.forEach((button) => {
    button.addEventListener("click", (e) => {
      const id = e.target.getAttribute("data-id");
      const bookIndex = booksData.findIndex((book) => book.id === parseInt(id));
      if (bookIndex !== -1) {
        const title = booksData[bookIndex].title;
        const author = booksData[bookIndex].author;

        booksData.splice(bookIndex, 1);

        alert(`Buku ${title} karya ${author} berhasil dihapus!`);

        localStorage.setItem("booksData", JSON.stringify(booksData));
        renderBookList();
      }
    });
  });

  const searchBook = document.getElementById("searchBook");
  searchBook.addEventListener("submit", (e) => {
    e.preventDefault();
    const keyword = document.getElementById("searchBookTitle").value;
    const searchResult = booksData.filter((book) =>
      book.title.toLowerCase().includes(keyword.toLowerCase())
    );

    renderBookList(searchResult);
  });
};

renderBookList();
